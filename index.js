//snapTravel connectivity task
"use strict";

let XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
let parseString = require('xml2js').parseString;

//1. command line arguments: City string, Checkin string, Checkout string
let city_string_input = process.argv[2];
let checkin_string_input = process.argv[3];
let checkout_string_input = process.argv[4];

//2. http post requests
//to  'https://experimentation.getsnaptravel.com/interview/hotels'

function firstPost(){
  let body = {
    city : city_string_input,
    checkin : checkin_string_input,
    checkout : checkout_string_input,
    provider : 'snaptravel'
  }
  let result1 = "";

  let xhr = new XMLHttpRequest();
  let url = "https://experimentation.getsnaptravel.com/interview/hotels";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
          result1 = JSON.parse(xhr.responseText);
          //console.log(result1);
          return result1;
      }
  };

  let data = JSON.stringify(body);
  xhr.send(data);
}

//to 'https://experimentation.getsnaptravel.com/interview/legacy_hotels'
function secondPost(){
  let body2 = '<?xml version="1.0" encoding="UTF-8"?>' +
              '<root>' +
                '<checkin>' + checkin_string_input + '</checkin>' +
                '<checkout>' + checkout_string_input + '</checkout>' +
                '<city>' + city_string_input + '</city>' +
                '<provider>snaptravel</provider>' +
              '</root>';

  let xhr2 = new XMLHttpRequest();
  let url2 = "https://experimentation.getsnaptravel.com/interview/legacy_hotels";
  xhr2.open("POST", url2);
  xhr2.setRequestHeader("Content-Type", "text/xml");
  xhr2.onreadystatechange = function () {
      if (xhr2.readyState === 4 && xhr2.status === 200) {
          parseString(xhr2.responseText, function (err, result) {
            //console.dir(result.root.element[0].id[0]);
          });
      }
  };

  xhr2.send(body2);
}

//3. compare both requests
function compareResult(firstPost, secondPost)
{
  //loop through results of first and compare with second
  //for (let i = 0; i < firstPost.length; i++) {
  //  for (let j = 0; j < secondPost.length; j++) {
  //    if (firstPost[i].id === secondPost[j].id) {
  //      //save in db
  //    }
  //  }
  //}
}

function myFunc(callback)
{
    callback();
}

myFunc(compareResult, firstPost, secondPost);

